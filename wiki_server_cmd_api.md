# список команд для комунікації сервер - адмін
## усі команди передаються з параметром **"raw"!**
## відповідь **"ok"** на команду яка змінює щось на сервері, означанє, що команда завершилася успішно
### версія **1.0**

1. **"get_all_register_commands"** - список усіх зареєстрованих команд на сервері. 
приклад запуску команди http://localhost:8091/?com=get_all_register_commands&raw
відповідь на виконання команди:
>[{ "name" : "get_self_pid", "description" : "Get self pid" },
 { "name" : "stop", "description" : "Stop server" },
 { "name" : "get_global_feature_states", "description" : "Get feature states" },
 { "name" : "get_config", "description" : "Get config" }]

2. **"get_config"** - отримати список усіх конфігурацій для сервера
приклад запуску команди http://localhost:8091/?com=get_config&raw
відповідь на виконання команди:
> [{'key':'model_version', 'value':'model_version 6.0, static_data_version 21'},
 {'key':'ip', 'value':'localhost'}, {'key':'port', 'value':'12345'},
 ...
 {'key':'global_log_level', 'value':'Debug'}]
 
3. **"get_log_levels"** - список усіх можливих рівнів логування
 приклад запуску команди http://localhost:8091/?com=get_log_levels&raw
 відповідь на виконання команди:
>Trace
>Debug
>... 
>Off

4. **"set_global_log_level"** - змінити глобальне нашатування логування для усього сервера
 приклад запуску команди http://localhost:8091/?com=set_global_log_level&global_log_level=Info&raw
 відповідь на виконання команди:
 команда завершилася успішно
> ok
 команда завершилася невдало 
>{'err_msg':'arg: global_log_level not found'}
>{'err_msg':'Invalid value global_log_level=Info22'}
 
5. **"get_global_feature_states"** - список усіх фіч які можливі на сервері, і також їхній статус( вкл./ викл.)
 приклад запуску команди http://localhost:8091/?com=get_global_feature_states&raw
>відповідь на виконання команди:
[{'feature_id':'CAMPAIGN', action_name:'ENABLE', 'action':'http://localhost:8091/?com=set_global_feature_state&feature_name=CAMPAIGN'},
 ...
 {'feature_id':'REDEEM_CODE', action_name:'DISABLE', 'action':'http://localhost:8091/?com=set_global_feature_state&feature_name=REDEEM_CODE'}]
 
6. **"set_global_feature_state"** - змінити глобально стейт фічі для усіх користувачів
 приклад запуску команди http://localhost:8091/?com=set_global_feature_state&raw
 відповідь на виконання команди:
 команда завершилася успішно
> ok
команда завершилася невдало 
> {'err_msg':'arg: feature_name not found'}
> {'err_msg':'Invalid value feature_name=www'}
 
 7. **"get_person_feature_states"** - список усіх фіч які можливі на сервері, для конкретного клієнта і також їхній статус( вкл./ викл.)
приклад запуску команди http://localhost:8091/?com=get_person_feature_states&raw
відповідь на виконання команди:
команда завершилася успішно
>[{'feature_id':'CAMPAIGN', action_name:'ENABLE', 'action':'http://localhost:8091/?com=set_person_feature_state&feature_name=CAMPAIGN&person_id=1'},
 ...
 {'feature_id':'REDEEM_CODE', action_name:'DISABLE', 'action':'http://localhost:8091/?com=set_person_feature_state&feature_name=REDEEM_CODE&person_id=1'}]
 
 > Not found person_id=1
 
 команда завершилася невдало 
> {'err_msg':'arg: person_id not found'}
 
8. **"set_person_feature_state"** - змінити глобально стейт фічі для усіх користувачів
 приклад запуску команди http://localhost:8091/?com=set_person_feature_state&feature_name=HERO&person_id=1&raw
 відповідь на виконання команди:
 команда завершилася успішно
> ok
команда завершилася невдало 
> {'err_msg':'arg: feature_name not found'}
> {'err_msg':'Invalid value feature_name=www'}
> {'err_msg':'arg: person_id not found'}
> {'err_msg':'Invalid value person_id=a'}
 
9. **"stop"** - зупиняє сервер
приклад запуску команди http://localhost:8091/?com=stop&raw

10. **"get_self_pid"** - повертає PID процесу
приклад запуску команди http://localhost:8091/?com=get_self_pid&raw
відповідь на виконання команди:
> 21256

11. **"get_api_version"** - повертає версію апі, яка використовується на сервері
приклад запуску команди http://localhost:8091/?com=get_api_version&raw
відповідь на виконання команди:
> 1.0
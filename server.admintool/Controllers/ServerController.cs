﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using System.Diagnostics;
using server.admintool.Models;
using server.admintool.Services;
using System;


namespace server.admintool.Controllers
{
  public class ServerController : Controller
  {
    private readonly ILogger<ServerController> _logger;
    private readonly ServerService _server_service;
    private static readonly Version _api_version = new Version( 1, 0 );

    public ServerController( ILogger<ServerController> logger, ServerService server_service )
    {
      _logger = logger;
      _server_service = server_service;
    }


    public IActionResult Index()
    {
      return View();
    }

    [HttpPost]
    public IActionResult ServerMenu( string authority )
    {
      Version server_api_verion = _server_service.getApiVersion( authority );
      if ( server_api_verion != _api_version )
        return View( new ErrorViewModel { error_message = $"admin api verion: {_api_version.ToString( 2 )}, server api version: {server_api_verion.ToString( 2 )}" } );

      TempData[CommonConstants.HOST_FIELD_NAME] = authority;
      return View();
    }

    [ResponseCache( Duration = 0, Location = ResponseCacheLocation.None, NoStore = true )]
    public IActionResult Error()
    {
      return View( new ErrorViewModel { request_id = Activity.Current?.Id ?? HttpContext.TraceIdentifier } );
    }

    public IActionResult GetSelfPid()
    {
      return View();
    }

    public IActionResult GetGlobalFeatureStates()
    {
      return View();
    }

    [HttpPost]
    public IActionResult GetGlobalFeatureStates( string feature_action )
    {
      string payload = _server_service.switchFeatureState( feature_action );
      if ( payload != CommonConstants.RESPONSE_CMD_COMPLETED_SUCCESSFULLY )
        return RedirectToAction( "Error" );

      return RedirectToAction( "GetGlobalFeatureStates" );
    }

    public IActionResult Stop()
    {
      _server_service.stopServer( TempData[CommonConstants.HOST_FIELD_NAME].ToString() );
      return RedirectToAction( "Index" );
    }

    public IActionResult GetConfig()
    {
      return View( "GetServerConfig" );
    }

    public IActionResult SetGlobalLogLevel( string log_level )
    {
      _server_service.setGlobalLogLevel( TempData[CommonConstants.HOST_FIELD_NAME].ToString(), log_level );
      return RedirectToAction( "GetConfig" );
    }

    public IActionResult FindPerson()
    {
      return View();
    }

    public IActionResult ReloadPersonInfo( string feature_action )
    {
      string person_id = TempData.Peek( CommonConstants.ARG_PERSON_ID )?.ToString();
      if ( string.IsNullOrEmpty( person_id ) )
        return RedirectToAction( "FindPerson" );

      string payload = _server_service.switchFeatureState( feature_action );
      if ( payload != CommonConstants.RESPONSE_CMD_COMPLETED_SUCCESSFULLY )
        return RedirectToAction( "Error" );

      return View( "ShowPersonInfo", person_id );
    }

    public IActionResult ShowPersonInfo( string person_id )
    {
      if ( string.IsNullOrEmpty( person_id ) )
      {
        person_id = TempData[CommonConstants.ARG_PERSON_ID]?.ToString();
        if ( string.IsNullOrEmpty( person_id ) )
          return RedirectToAction( "FindPerson" );
      } else
        TempData[CommonConstants.ARG_PERSON_ID] = person_id;

      return View( (object)person_id );
    }
  }
}

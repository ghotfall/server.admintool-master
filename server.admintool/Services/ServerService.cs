﻿using Microsoft.Extensions.Caching.Memory;
using System;
using System.Collections.Generic;
using System.Net;
using System.Text;
using server.admintool.Models;
using Newtonsoft.Json;


namespace server.admintool.Services
{
  public class ServerService
  {
    #region commands
    private const string CMD_GET_API_VERSION ="?com=get_api_version";
    private const string CMD_GET_ALL_COMMANDS = "?com=get_all_register_commands";
    private const string CMD_SERVER_PID = "?com=get_self_pid";
    private const string CMD_GET_GLOBAL_FEATURE_STATES = "?com=get_global_feature_states";
    private const string CMD_GET_PERSON_FEATURE_STATES = "?com=get_person_feature_states";
    private const string CMD_SERVER_STOP = "?com=stop";
    private const string CMD_GET_CONFIG = "?com=get_config";
    private const string CMD_GET_LOG_LEVELS = "?com=get_log_levels";
    private const string CMD_SET_GLOBAL_LOG_LEVEL = "?com=set_global_log_level";
    #endregion

    private IMemoryCache _cache;


    public ServerService( IMemoryCache memory_cache )
    {
      _cache = memory_cache;
    }


    public List<CommandInfo> getCommandInfosForHost( string host )
    {
      if ( string.IsNullOrEmpty( host ) )
        throw new Exception();

      List<CommandInfo> command_infos = null;

      if ( !_cache.TryGetValue( host, out command_infos ) )
      {
        string payload = getRawDataByUrl( $"{host}{CMD_GET_ALL_COMMANDS}" );

        validatePayload( payload );

        command_infos = new List<CommandInfo>();

        CommandInfo[] commands = JsonConvert.DeserializeObject<CommandInfo[]>( payload );
        for ( int i = 0; i < commands.Length; i++ )
        {
          commands[i].action = getActionName( commands[i].name );
          command_infos.Add( commands[i] );
        }

        _cache.Set( host, command_infos, new MemoryCacheEntryOptions().SetAbsoluteExpiration( TimeSpan.FromMinutes( 20 ) ) );
      }

      return command_infos;

      string getActionName( string cmd_name )
      {
        string[] words = cmd_name.Split( "_" );
        StringBuilder sb = new StringBuilder( words.Length );
        for ( int i = 0; i < words.Length; i++ )
        {
          char[] n = words[i].ToCharArray();
          n[0] = char.ToUpper( n[0] );
          sb.Append( new string( n ) );
        }
        return sb.ToString();
      }
    }

    public Version getApiVersion( string host )
    {
      string payload = getRawDataByUrl( $"{host}{CMD_GET_API_VERSION}" );

      validatePayload( payload );

      return new Version( payload );
    }

    public string getServerPid( string host )
    {
      string payload = getRawDataByUrl( $"{host}{CMD_SERVER_PID}" );

      validatePayload( payload );

      return payload;
    }

    public List<FeatureInfo> getFeatureStates( string host, string person_id = "" )
    {
      string url = !string.IsNullOrEmpty( person_id )
                ? $"{host}{CMD_GET_PERSON_FEATURE_STATES}&{CommonConstants.ARG_PERSON_ID}={person_id}"
                : $"{host}{CMD_GET_GLOBAL_FEATURE_STATES}";

      string payload = getRawDataByUrl( url );

      validatePayload( payload );

      if ( payload.StartsWith( CommonConstants.NOT_FOUND ) )
        return new List<FeatureInfo>();

      List<FeatureInfo> features = JsonConvert.DeserializeObject<List<FeatureInfo>>( payload );
      return features;
    }

    public string switchFeatureState( string url )
    {
      string payload = getRawDataByUrl( url );

      validatePayload( payload, true );

      return payload;
    }

    public void stopServer( string host )
    {
      getRawDataByUrl( $"{host}{CMD_SERVER_STOP}" );
    }

    public List<ConfigItem> getConfig( string host )
    {
      string payload = getRawDataByUrl( $"{host}{CMD_GET_CONFIG}" );

      validatePayload( payload );

      return JsonConvert.DeserializeObject<List<ConfigItem>>( payload );
    }

    public string[] getServerLogLevels( string host )
    {
      string payload = getRawDataByUrl( $"{host}{CMD_GET_LOG_LEVELS}" );

      validatePayload( payload );

      return payload.Split( $"{Environment.NewLine}" );
    }

    public void setGlobalLogLevel( string host, string log_level )
    {
      string payload = getRawDataByUrl( $"{host}{CMD_SET_GLOBAL_LOG_LEVEL}&{CommonConstants.ARG_GLOBAL_LOG_LEVEL}={log_level}" );

      validatePayload( payload, true );
    }

    private void validatePayload( string payload, bool is_set_cmd = false )
    {
      if ( string.IsNullOrEmpty( payload ) )
        throw new Exception( $"{nameof( payload )} is null or empty!" );

      dynamic err_msg = new { err_msg = string.Empty };
      try
      {
        err_msg = JsonConvert.DeserializeAnonymousType( payload, err_msg );
      } finally
      {
        if ( !string.IsNullOrEmpty( err_msg.err_msg ) || (is_set_cmd && payload != CommonConstants.RESPONSE_CMD_COMPLETED_SUCCESSFULLY) )
          throw new Exception( err_msg.err_msg );
      }
    }

    private string getRawDataByUrl( string url )
    {
      url += CommonConstants.AND_ARG_RAW;
      using ( WebClient client = new WebClient { Encoding = Encoding.UTF8 } )
      {
        try
        {
          return client.UploadString( url, string.Empty );
        } catch ( WebException ex )
        {
          return null;
        }
      }
    }
  }
}

namespace server.admintool.Models
{
  public class ErrorViewModel
  {
    public string request_id { get; set; }
    public string error_message { get; set; }


    public bool showRequestId => !string.IsNullOrEmpty( request_id );
  }
}

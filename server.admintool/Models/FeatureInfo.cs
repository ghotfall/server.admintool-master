﻿namespace server.admintool.Models
{
  public class FeatureInfo
  {
    public string feature_id { get; set; }
    public string action_name { get; set; }
    public string action { get; set; }
  }
}

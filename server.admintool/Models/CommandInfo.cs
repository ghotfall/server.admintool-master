﻿namespace server.admintool.Models
{
  public class CommandInfo
  {
    public string name { get; set; }
    public string description { get; set; }
    public string action { get; set; }
  }
}

﻿namespace server.admintool.Models
{
  public class ConfigItem
  {
    public string key { get; set; }
    public string value { get; set; }
  }
}

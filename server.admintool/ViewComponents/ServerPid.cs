﻿using Microsoft.AspNetCore.Mvc;
using server.admintool.Services;


namespace server.admintool.ViewComponents
{
  public class ServerPid : ViewComponent
  {
    private readonly ServerService _server_service;


    public ServerPid( ServerService server_service )
    {
      _server_service = server_service;
    }


    public IViewComponentResult Invoke()
    {
      object server_pid = _server_service.getServerPid( TempData.Peek( CommonConstants.HOST_FIELD_NAME ).ToString() );
      return View( server_pid );
    }
  }
}

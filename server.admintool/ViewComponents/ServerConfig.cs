﻿using Microsoft.AspNetCore.Mvc;
using System.Collections.Generic;
using server.admintool.Models;
using server.admintool.Services;


namespace server.admintool.ViewComponents
{
  public class ServerConfig : ViewComponent
  {
    private readonly ServerService _server_service;


    public ServerConfig( ServerService server_service )
    {
      _server_service = server_service;
    }


    public IViewComponentResult Invoke()
    {
      List<ConfigItem> config = _server_service.getConfig( TempData.Peek( CommonConstants.HOST_FIELD_NAME ).ToString() );
      ViewBag.curLogLevel = config.Find( ci => ci.key == CommonConstants.ARG_GLOBAL_LOG_LEVEL ).value;
      return View( config );
    }
  }
}

﻿using Microsoft.AspNetCore.Mvc;
using System.Collections.Generic;
using server.admintool.Models;
using server.admintool.Services;


namespace server.admintool.ViewComponents
{
  public class FeatureStates : ViewComponent
  {
    private readonly ServerService _server_service;


    public FeatureStates( ServerService server_service )
    {
      _server_service = server_service;
    }


    public IViewComponentResult Invoke( string person_id = "" )
    {
      List<FeatureInfo> features = _server_service.getFeatureStates( TempData.Peek( CommonConstants.HOST_FIELD_NAME ).ToString(), person_id );

      if ( features.Count == 0 )
        return View( CommonConstants.VIEW_COMPONENT_NAME_NOT_FOUND );

      ViewBag.feature_action_name = string.IsNullOrEmpty( person_id ) ? "GetGlobalFeatureStates" : "ReloadPersonInfo";

      return View( features );
    }
  }
}

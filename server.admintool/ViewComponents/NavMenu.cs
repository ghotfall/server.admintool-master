﻿using Microsoft.AspNetCore.Mvc;
using System.Collections.Generic;
using server.admintool.Models;
using server.admintool.Services;


namespace server.admintool.ViewComponents
{
  public class NavMenu : ViewComponent
  {
    private readonly ServerService _server_service;


    public NavMenu( ServerService server_service )
    {
      _server_service = server_service;
    }


    public IViewComponentResult Invoke()
    {
      List<CommandInfo> commands = _server_service.getCommandInfosForHost( TempData.Peek( CommonConstants.HOST_FIELD_NAME ).ToString() );
      return View( commands );
    }
  }
}

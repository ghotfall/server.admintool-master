﻿using Microsoft.AspNetCore.Mvc;
using System.Collections.Generic;
using System.Linq;
using server.admintool.Services;


namespace server.admintool.ViewComponents
{
  public class ServerLogLevels : ViewComponent
  {
    private readonly ServerService _server_service;


    public ServerLogLevels( ServerService server_service )
    {
      _server_service = server_service;
    }


    public IViewComponentResult Invoke()
    {
      IEnumerable<string> log_levels = _server_service.getServerLogLevels( TempData.Peek( CommonConstants.HOST_FIELD_NAME ).ToString() );
      return View( log_levels.Where( l => !string.IsNullOrEmpty( l ) ) );
    }
  }
}

﻿namespace server.admintool
{
  public class CommonConstants
  {
    public const string ARG_PERSON_ID = "person_id";
    public const string ARG_FEATURE_NAME = "feature_name";
    public const string ARG_GLOBAL_LOG_LEVEL = "global_log_level";
    public const string AND_ARG_RAW = "&raw";

    public const string NOT_FOUND = "Not found";
    public const string RESPONSE_CMD_COMPLETED_SUCCESSFULLY = "ok";

    public const string HOST_FIELD_NAME = "host";

    public const string VIEW_COMPONENT_NAME_NOT_FOUND = "NotFound";
  }
}
